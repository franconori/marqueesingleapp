<?php
// Remove Emojis
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function marquee_script_enqueue(){
    /*Bootstap Style*/
    wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css', array(), '3', 'all');

    /*Font Awesome*/
    wp_enqueue_style('fontawesomecss', get_template_directory_uri() . '/libs/font-awesome/css/font-awesome.css', array(), '', 'all');


    wp_enqueue_style('wp-styles', get_template_directory_uri() . '/css/styles.css', array(), '1.0.0', 'all');
    /*Custom files*/
    wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/custom.css', array(), '1.0.0', 'all');

    // /*jQuery*/
    // wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jquery-1.11.1.min.js', '1.11.1', 'all');
    //
    // /*Bootstrap JS*/
    // wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.min.js', '3', true);
    //
    // /*Parallax Lib*/
    // wp_enqueue_script('parallaxjs', get_template_directory_uri() . '/libs/parallax/parallax.min.js', '', 'all');

}

add_action('wp_enqueue_scripts', 'marquee_script_enqueue');

function marquee_theme_setup(){

    add_theme_support('menus');

    register_nav_menu('primary','Primary Header Navigation');
}

add_action('init', 'marquee_theme_setup');

/* Marque Navigation */
function marquee_nav()
{
    wp_nav_menu(
        array(
            'theme_location'  => 'header-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'container',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '
            <div class="collapse navbar-collapse" id="navbar-collapse-header">
                <ul class="nav navbar-nav">%3$s</ul>
            </div>',
            'depth'           => 0,
            'walker'          => ''
        )
    );
}
