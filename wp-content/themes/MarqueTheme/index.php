<?php get_header(); ?>

  <!-- First Section -->
  <div class="container-fluid introduction" id="introduction">
      <div class="container">
          <div class="row background">
              <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                  <h1>
                      <span>A Member Driven Social Club</span>
                  </h1>
                  <h4>
                      <span>The opportunities are endless.</span>
                  </h4>
                  <p class="text-center btn">
                      <i class="fa fa-long-arrow-right" aria-hidden="true"></i> BECOME A MEMBER
                  </p>
              </div>
          </div>
          <div class="row chevron-arrow">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
          </div>
      </div>
  </div>

  <!-- Second Section -->
  <div class="container-fluid about" id="about">
      <div class="container">
          <div class="row about-text">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <p class="text-center">
                      The Marque first unveiled its secrets in 2012 and has been growing ever since. It is an amazing 15,000 square feet of Baroque inspired and posh styled rooms to culminate in Houston's most opulent Business Social Club. Strategically located in City Centre between downtown and the Energy Corridor, it is a perfect distance to entertain clients, get out of the city for a fine dining escape or to host an out-of-this world event. Join us at The Marque as we continue our quest to make this masterpiece a work of perfection
                      <a href="" class="learn-more-btn btn">Learn More</a>
                  </p>
              </div>
          </div>
      </div>
  </div>
  <div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri(); ?>/img/preview-home.jpg">
      <div class="container-fluid cite" id="cite">
          <div class="container">
              <div class="row cite-text">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <h2 class="text-center">
                              <em>
                              When I was looking for an exclusive club in Houston, I wasn't expecting to a find a luxurious, New York style, lounge. There's a presence here that is unprecedented, unlike anything that I've seen in Texas before
                              </em>
                          </h2>
                          <footer class="text-center">
                              <cite title="Source Title"> - WITH LOVE, FABIO</cite>
                          </footer>
                      <p class="text-center">

                      </p>
                      <p class="text-center">
                      </p>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- Third Section -->
  <div class="container-fluid the-rooms" id="the-rooms">
      <div class="container">
          <div class="row title">
              <h2 class="text-uppercase text-center">
                  "The Rooms"
              </h2>
          </div>
          <div class="row rooms">
              <div class="col-lg-3 col-md-3 col-sm-4-col-xs-6">
                  <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cigar-lounge.png" alt="">
                  <h4>
                      <span>01 /</span>
                      <p class="text-uppercase"> Octane Lounge</p>
                  </h4>
                  <p class="text-justify">
                      Minions ipsum jiji ti aamoo! Po kass aaaaaah jeje jeje daa gelatooo la bodaaa. Bappleees aaaaaah aaaaaah bappleees tulaliloo tulaliloo bee do bee do bee do.
                  </p>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4-col-xs-6">
                  <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cigar-lounge.png" alt="">
                  <h4>
                      <span>02 /</span>
                      <p class="text-uppercase"> Wine Cellar</p>
                  </h4>
                  <p class="text-justify">
                      Minions ipsum jiji ti aamoo! Po kass aaaaaah jeje jeje daa gelatooo la bodaaa. Bappleees aaaaaah aaaaaah bappleees tulaliloo tulaliloo bee do bee do bee do.
                  </p>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4-col-xs-6">
                  <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cigar-lounge.png" alt="">
                  <h4>
                      <span>03 /</span>
                      <p class="text-uppercase"> Tequila Library</p>
                  </h4>
                  <p class="text-justify">
                      Minions ipsum jiji ti aamoo! Po kass aaaaaah jeje jeje daa gelatooo la bodaaa. Bappleees aaaaaah aaaaaah bappleees tulaliloo tulaliloo bee do bee do bee do.
                  </p>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-4-col-xs-6">
                  <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cigar-lounge.png" alt="">
                  <h4>
                      <span>04 /</span>
                      <p class="text-uppercase"> Havana Cigar Lounge</p>
                  </h4>
                  <p class="text-justify">
                      Minions ipsum jiji ti aamoo! Po kass aaaaaah jeje jeje daa gelatooo la bodaaa. Bappleees aaaaaah aaaaaah bappleees tulaliloo tulaliloo bee do bee do bee do.
                  </p>
              </div>
          </div>
      </div>
  </div>
  <div class="container-fluid tour">
      <div class="container">
          <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                  <h2 class="text-uppercase text">
                      <span>
                      Become an exclusive member, socialize with houston's busines owners
                      </span>
                  </h2>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <button class="schedule-btn btn text-uppercase">
                      Schedule a private tour
                  </button>
              </div>
          </div>
      </div>
  </div>

  <!-- Forth Section -->
  <div class="container-fluid contact" id="contact">
      <div class="container">
          <div class="row data">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                  <span class="glyphicon glyphicon-phone-alt"></span>
                  <h2 class="text-center">832.726.1930</h2>
                  <h5 class="text-center text-uppercase">
                      <i class="fa fa-envelope-o" aria-hidden="true"></i> Inquiries@MarqueHouston.com
                      <i class="fa fa-map-marker" aria-hidden="true"></i> Houston, Texas 77024
                  </h5>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></div>
          </div>
      </div>
  </div>
  <div class="container-fluid form-contact" id="form-contact">
      <div class="container">
          <div class="row contact-form">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-0"></div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                  <h3 class="text-center text-uppercase">Inquiries</h3>
                  <form>
                      <div class="form-group">
                          <input type="text" class="form-control" id="name" placeholder="Your Name">
                      </div>
                      <div class="form-group">
                          <input type="email" class="form-control" id="email" placeholder="Your Email">
                      </div>
                      <div class="form-group">
                          <textarea class="form-control" rows="8" placeholder="Your Message"></textarea>
                      </div>
                      <button type="submit" class="btn btn-default">Submit</button>
                  </form>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-0"></div>
          </div>
      </div>
  </div>

<?php get_footer(); ?>
