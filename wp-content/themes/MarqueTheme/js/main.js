$(document).ready(function() {

  // Scrolled Script
  var scrolled = false;
  $(window).scroll(function() {
      if (($(window).scrollTop() != 0)&&(!scrolled)) {
          $('#up-arrow').fadeIn(500);
          $('#navbar-top').removeClass('original').addClass('different');
      }
      else if($(window).scrollTop()==0)
      {
          $('#up-arrow').fadeOut(500);
          $('#navbar-top').removeClass('different').addClass('original');
      }
  });


  // Up Arrow function
  $('#up-arrow').click(function(){
      scrolled = true;
      $('html,body').animate({
              scrollTop: 0
          },
          800,
          'swing',
          function() {
              scrolled = false;
          }
      );
  });




});
