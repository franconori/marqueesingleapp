    <?php
    if(is_front_page())
    {
    ?>
        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                <a id="up-arrow" class="chevron-up-arrow" href="#">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="envelope" href="#">
                    <i class="fa fa-envelope-o"></i>
                </a>
            </div>
            </div>
        </nav>
    <?php
    }
    ?>




    <!--jQuery-->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.11.1.min.js"></script>
    <!--Bootstrap-->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/libs/bootstrap/js/bootstrap.min.js"></script>
    <!--Parallax-->
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/libs/parallax/parallax.min.js"></script>

    <!-- Custom Main JS -->

    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

    <!-- Function to get the user to the top of the page -->

    <?php wp_footer(); ?>
</body>
</html>
