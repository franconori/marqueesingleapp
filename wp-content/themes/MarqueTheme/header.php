<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Marque</title>

        <!-- This is good, we always need a wp_head, but it should always be at the bottom of the head -->
        <?php wp_head(); ?>
    </head>

    <?php
      if(is_front_page())
      {
          $marque_classes = array('marque-class', 'home-class');
      }
      else
      {
          $marque_classes = array('no-marque-class');
      }
    ?>
    <body <?php body_class($marque_classes); ?>>
        <!-- Navigation Bar -->
        <nav class="navbar navbar-inverse navbar-fixed-top original" id="navbar-top">
            
            <?php marquee_nav(); ?>
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-header" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img class="img-responsive brand" src="<?php echo get_template_directory_uri() . '/img/marquehouston_draft.png' ?>" alt="">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse-header">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="text-uppercase" href="#">
                                Introduction
                            </a>
                        </li>
                        <li>
                            <a class="text-uppercase" href="#">
                                About
                            </a>
                        </li>
                        <li>
                            <a class="text-uppercase" href="#">
                                Membership
                            </a>
                        </li>
                        <li>
                            <a class="text-uppercase" href="#">
                                Gallery
                            </a>
                        </li>
                        <li>
                            <a class="text-uppercase" href="#">
                                Events
                            </a>
                        </li>
                        <li>
                            <a class="text-uppercase" href="#">
                                Contact
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
